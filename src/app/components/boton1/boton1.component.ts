import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/servicios/data.service';

@Component({
  selector: 'app-boton1',
  templateUrl: './boton1.component.html',
  styleUrls: ['./boton1.component.css']
})
export class Boton1Component implements OnInit {

  textoBoton1: string = 'Hola Botón 1!'
  constructor(private ServicioTitle : DataService) { }

  ngOnInit(): void {
  }

  cambiarTexto():void{
    this.ServicioTitle.texto = this.textoBoton1
  }
}
