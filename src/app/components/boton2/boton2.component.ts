import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/servicios/data.service';

@Component({
  selector: 'app-boton2',
  templateUrl: './boton2.component.html',
  styleUrls: ['./boton2.component.css']
})
export class Boton2Component implements OnInit {

  textoBoton2: string = 'Hola Botón 2!';
  constructor(private ServicioTitle: DataService) { }

  ngOnInit(): void {
  }

  cambiartexto(): void{
    this.ServicioTitle.texto = this.textoBoton2
  }
}
